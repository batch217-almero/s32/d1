let http = require("http");

// MOCK DATABASE
let directory = [
	{
		"name" : "Brandon",
		"email" : "brandon@mail.com"
	},
	{
		"name" : "Jobart",
		"email" : "jobart@mail.com"
	}
]

let port = 4000;

let server = http.createServer((req, res) =>{
	if(req.url== "/users" && req.method == "GET"){
		res.writeHead(200, {"Content-Type" : "application/json"});
		// stringify conversion
		res.write(JSON.stringify(directory));
		res.end();
	}

	if(req.url== "/users" && req.method == "POST"){
		// an empty string will act as place holder for resource/data to be created later on
		let requestBody = "";
	
		req.on("data", function(data){
			requestBody += data;//assign data retrieved and place in requestBody
		});

		req.on("data", function(){
			console.log(typeof requestBody);

			requestBody = JSON.parse(requestBody);

			let newUser = {
					"name" : requestBody.name,
					"email" : requestBody.email
			}
			
			directory.push(newUser);
			console.log(directory);

			res.writeHead(200, {"Content-Type" : "application/json"});
			res.write(JSON.stringify(newUser));
			res.end();
		});
	}

});

server.listen(port);
console.log(`Server is running at localhost: ${port}.`);