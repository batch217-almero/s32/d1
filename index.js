let http = require("http");

let port = 4000;

let server = http.createServer((req, res) => {
	// GET - HTTP Method: retrieve or read information
	if (req.url == "/items" && req.method == "GET") {
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Data retrieved from the database.");
	} 

	// POST - HTTP Method: insert data in the server DB
	if(req.url == "/items" && req.method == "POST"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Data to be sent to the database.");
	}
});

server.listen(port);
console.log(`Server is running at localhost: ${port}.`);